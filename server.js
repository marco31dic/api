//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  //res.send("Hola mundo node js");
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post("/",function(req,res){
  res.send("Hemos recibido su peticiòn post");
})

app.get("/clientes/:idcliente",function(req,res){
  //res.send("Aqui tienes al cliente nùmero: " + req.params.idcliente);
  var mijson = "{'idcliente':12345}";
  res.send(mijson);
})

app.put("/",function(req,res){
  res.send("Hemos recibido su peticiòn put cambiado");
})

app.delete("/",function(req,res){
  res.send("Hemos recibido su peticiòn delete");
})

app.get("/v1/movimientos",function(req,res){
  res.sendFile(path.join(__dirname, 'movimientosv1.json'));
})

var movimientosJSON = require('./movimientosv2.json');

app.get("/v2/movimientos",function(req,res){
  res.json(movimientosJSON);
})

app.get("/v2/movimientos/:id",function(req,res){
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id-1]);
})

app.get('/v2/movimientosq', function (req,res){
  console.log(req.query);
  res.send("recibido");
  //localhost:3000/v2/movimientosq?id=33&otrovalor=hola
})


var bodyparser = require('body-parser'); //parsear el json
app.use(bodyparser.json());
app.post('/v2/movimientos',function(req,res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("Movimiento dado de alta");
})
